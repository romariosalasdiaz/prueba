var matriz1;
var matrizRes;
var filaA;
var colA;
var filaB;
var colB;
var size;
var contador = 0;
//contador cuenta las veces que se accede, para que si no es la primera, se borren las capas creadas previamente.


//FUNCIONCES QUE DEBEMOS DEFINIR PARA CREAR LAS MATRICES
function CreaMatriz(n, m) {
	//DEFINIMOS EL TAMAÑO DE LA MATRIZ
	this.length = n;
	for (var i=0; i<n; i++) {
		this[i] = new Array(m);
	}
	return this;
}

function Multiplicar () {
   
	for (i=0; i < size; i++){
		for (j=0; j < size; j++){			
			matrizRes[i][j] = matrizRes[i][j] + (matriz1[i][j] * 2);
		}
    }
    Mostrar()
}

function Mostrar () {
	
	var q = 0;	
	for (i=0; i < matrizRes.length; i++){
		for (j=0; j < matrizRes.length; j++){
			document.matrizA.elements[q].value = matrizRes[i][j];
			matrizRes[i][j] = 0;
			q++;
		}
	}
}

//Esta función recoge los datos del formulario y los guarda en las matrices
function Cargar () {
    var q = 0;    
	for (i=0; i<size; i++) {
		for (j=0; j<size; j++) {
            if(isNaN(parseInt(document.matrizA.elements[q].value))){
                alert("Valores no v\u00e1lidos.");
            }        
			matriz1[i][j] = parseInt(document.matrizA.elements[q].value);
			q++;
		}
    }  
		
	Multiplicar ()
}

//Comprueba que las dimensiones de las matrices son correctas para poder multiplicarlas
function Comprobar () { 

    if (contador > 0) {
        Borrar ()
    }
    size = parseInt(document.dimB.elements[0].value);
    if(isNaN(parseInt(size))){
        alert("digite un valor para N."); 
    }else{
        matriz1 = new CreaMatriz(size, size);
        matrizRes = new CreaMatriz(size, size);
        CrearFormularios (size) 
        cargaMatrixAleatoria(size)
        contador++
    }   
  	
}

function cargaMatrixAleatoria (size) {
    Inicializar ()
    var nMaxAle = size * size;
    console.log(nMaxAle);
  
    for (i=0; i<size; i++) {
		for (j=0; j<size; j++) {        
			matrizRes[i][j] = Math.floor(Math.random() * (2*size+1 - size)) + size ;			
		}
    } 
    Mostrar()
}

function CrearFormularios (size) {

	var d = document.createElement("DIV");
	var fA = document.createElement("FORM");
    var A = document.createTextNode("Matriz");

	d.setAttribute("id", "matrices");
	d.setAttribute("align", "center");
	d.setAttribute("style", "width: 50%; height: 100%; float: left; background-color: 66FF66");
	fA.setAttribute("name", "matrizA");
	
	var boton = document.createElement("INPUT");
	boton.setAttribute("type", "button");
	boton.setAttribute("value", "Multiplicador");
	boton.setAttribute("name", "button");
	boton.onclick=function(){Cargar();}
	
	for (i=0; i<size; i++) {
		var salto = document.createElement("BR");
		for (j=0; j<size; j++) {
			var casilla = document.createElement("INPUT");
			casilla.setAttribute("type","text");
			casilla.setAttribute("size","4");
			casilla.setAttribute("name","text");
			fA.appendChild(casilla);
		}
		fA.appendChild(salto);
	}
	var salto = document.createElement("BR");
	d.appendChild(salto);

	d.appendChild(A);
	d.appendChild(fA);
	var salto = document.createElement("BR");
	d.appendChild(salto);
	d.appendChild(boton);
	
	var otro = document.getElementById("main");
	otro.appendChild(d);
}

function Inicializar () {
	for (i=0; i < matrizRes.length; i++){
		for (j=0; j < matrizRes.length; j++){
			matrizRes[i][j] = 0;
		}
	}
}

function Borrar () {
	var capa1 = document.getElementById("matrices");
	var capa2 = document.getElementById("resultado");
	var padre1 = capa1.parentNode;
	var padre2 = capa2.parentNode;
	padre1.removeChild(capa1);
	padre2.removeChild(capa2);
}
